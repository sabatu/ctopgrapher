﻿using CTographer.Models.Class_Components;
using System;
using System.Collections.Generic;
using System.Text;

namespace CTographer.Models
{
    public class ClassFile
    {
        public string ClassName { get; set; }
        public List<Method> ClassMethods { get; set; }
        public List<Variable> ClassVariables { get; set; }

    }
}
