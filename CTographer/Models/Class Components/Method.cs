﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTographer.Models.Class_Components
{
    public class Method
    {
        public string MethodName { get; set; }
        public List<Variable> InputParameters { get; set; }
        public Variable ReturnValue { get; set; }

    }
}
